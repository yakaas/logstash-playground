#!/bin/bash -xe

docker kill $(docker ps -q)

docker run -t --rm \
	-v "$PWD":/config-dir \
	-v "$PWD/data":/data \
	logstash-156 logstash \
	-f /config-dir/logstash.conf \
	#--configtest
	#--debug \
	#--verbose


