#!/bin/bash -e

docker kill $(docker ps -q)

docker run -t --rm \
	-v "$PWD":/config-dir \
	-v "$PWD/data":/data \
	-p 9299:9299 \
	logstash-156 logstash \
	-f /config-dir/logstash.conf \
	#--configtest
	#--debug \
	#--verbose \

	